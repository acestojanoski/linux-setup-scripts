#!/bin/bash

git remote set-url origin https://github.com/acestojanoski/linux-setup-scripts.git
git push origin master
git remote set-url origin https://gitlab.com/acestojanoski/linux-setup-scripts.git
