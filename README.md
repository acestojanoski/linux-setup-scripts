# Linux Setup Scripts

This is a repository which contains scripts for setting up my linux installation.

Modify them to fit your needs.

## Cloning
`
git clone https://gitlab.com/acestojanoski/linux-setup-scripts.git && cd linux-setup-scripts && sudo chmod +x ./*.sh ./applications/*.sh ./theming/*.sh ./i3/*.sh
`

## Contents

*   Core packages:
    - terminator
    - neofetch
    - vlc
    - zip, unzip, rar, unrar
    - build-essential
    - vim 
    - rofi
    - shutter
    - meld
    - Chromium browser
    - libreoffice
    - gimp
    - dmenu
    - j4-dmenu-desktop
    - curl
    - apt-transport-https
    - transmission
*   Nvidia driver 390
*   MT7630E driver for mediatek wireless card
*   Theming
    - adapta GTK theme
    - arc theme
    - breeze cursor theme
    - papirus icons
    - sardi icons
    - powerline-shell
    - install-fonts
*   Applications
    - nodejs, npm
    - skype
    - Visual Studio Code
    - TeamViewer
    - Sublime
    - VirtualBox
    - Docker
    - Obs
    - Atom
*   i3
    - install dependencies script
    - install i3 and i3 core packages script
    - install i3-gaps script
*   vim-setup.sh - script for setting up vim

### Applications

You can change the URL of the .deb packages to install other versions of the applications
